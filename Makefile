SHELL = /bin/bash

depends = Monedas.py setup.py main.pyx second.pyx
objects = main_function.so secondary_function.so
build_directory = build
dist_directory = dist
install_directory = finished

all : $(depends)
	@echo -n "Creando directorios de build..."
	@mkdir $(dist_directory)
	@echo " Hecho."
	@echo "Compilando funciones principales..."
	@python setup.py build_ext --inplace
	@echo "Compilacion terminada."
	@cp M*.py $(dist_directory)
	@mv *.so $(dist_directory)
	@mv *.c $(build_directory)
	@echo "Build terminada."



install :
	@echo -n "Instalando en $(install_directory)... "
	@if [ -d "$(dist_directory)" ] ; then if [ -d "$(install_directory)" ]; then echo -e "\n$(install_directory) already exists. Aborting." ; else mkdir $(install_directory) ; cp $(dist_directory)/* $(install_directory)/ && echo "Done."; echo "All done." ; fi ; else echo -e "\nUnable to install because of missing build. Aborting." ; fi

.PHONY : clean
clean :
	@if [ -d "$(build_directory)" ] ; then rm -r $(build_directory) ; fi
	@if [ -d "$(dist_directory)" ] ; then rm -r $(dist_directory) ; fi
	@echo "Building directories cleaned."

uninstall : 
	@if [ -d "$(install_directory)" ] ; then rm -r $(install_directory) ; echo "Program uninstalled from the system." ; else echo "Program not installed." ; fi
