import numpy as np
cimport numpy as np
cimport cython
FLOAT = np.float64
ctypedef np.float64_t FLOAT_t
LONG = np.int64
ctypedef np.int64_t LONG_t

@cython.boundscheck(False)
def main(unsigned int runs):
    cdef unsigned int N = 1000
    cdef unsigned int timesfliped = 10
    cdef unsigned int everysinglerun
    cdef double v1, vrand, vmin, cmin
    cdef np.ndarray[FLOAT_t, ndim=2] results = np.empty((runs,3))
    cdef np.ndarray[LONG_t, ndim=2] globallist = np.zeros((N,timesfliped), dtype=np.int64)
    cdef np.ndarray[LONG_t, ndim=1] c1 = np.zeros((10), dtype=np.int64)
    cdef np.ndarray[LONG_t, ndim=1] crand = np.zeros((10), dtype=np.int64)
    cdef np.ndarray[LONG_t, ndim=1] sumofresultsinasinglecoin = np.zeros((N), dtype=np.int64)

    for everysinglerun in xrange(runs):
        globallist = np.random.randint(0,2,(N,timesfliped)) # 0 cruz 1 cara
    
        # picking the first coin in every run
        c1 = globallist[0]

        # picking a random coin
        crand = globallist[np.random.randint(0,N-1)]

        # picking the minimum out of N coins for each run
        sumofresultsinasinglecoin = np.sum(globallist,axis=1)
        cmin = np.amin(sumofresultsinasinglecoin, axis=0)

        v1 = np.sum(c1)/float(timesfliped)
        vrand = np.sum(crand)/float(timesfliped)
        vmin = cmin/float(timesfliped)

        results[everysinglerun,0] = v1
        results[everysinglerun,1] = vrand
        results[everysinglerun,2] = vmin

    cdef np.ndarray[FLOAT_t, ndim=1] final_results = np.mean(results,axis=0)
    v1 = final_results[0]
    vrand = final_results[1]
    vmin = final_results[2]

    print("Average value of vmin= %s" % vmin)
    print("Average value of v1= %s" % v1)
    print("Average value of vrand= %s" % vrand)

    return 0
