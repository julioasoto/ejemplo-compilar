import numpy as np
cimport numpy as np
cimport cython
DTYPE = np.int64
ctypedef np.int64_t DTYPE_t

@cython.boundscheck(False)
def main(unsigned int runs):
    cdef unsigned int N = 1000,
    cdef unsigned int timesfliped = 10
    cdef double v1, vrand, vmin
        
    cdef np.ndarray[DTYPE_t, ndim=3] globallist = np.random.randint(0,2,(runs,N,timesfliped)) # 0 cruz 1 cara
    ##globallist = np.random.randint(0,2,(runs,N,timesfliped)) # 0 cruz 1 cara

    # picking the first coin in every run
    cdef np.ndarray[DTYPE_t, ndim=2] c1 = globallist[:,0]
    ##c1 = globallist[:,0]
    
    # picking a random coin
    cdef np.ndarray[DTYPE_t, ndim=1] randompickperrun = np.random.randint(0,N,runs)
    ##randompickperrun = np.random.randint(0,N,runs)
    cdef np.ndarray[DTYPE_t, ndim=2] crand = globallist[np.arange(runs),randompickperrun,:]
    ##crand = globallist[np.arange(runs),randompickperrun,:]
    
    # picking the minimum out of N coins for each run
    cdef np.ndarray[DTYPE_t, ndim=2] sumofresultsinasinglecoin = np.sum(globallist,axis=2)
    ##sumofresultsinasinglecoin = np.sum(globallist,axis=2)
    cdef np.ndarray[DTYPE_t, ndim=1] cmin = np.amin(sumofresultsinasinglecoin, axis=1)
    ##cmin = np.amin(sumofresultsinasinglecoin, axis=1)

    v1=np.sum(np.sum(c1,1)/float(timesfliped))/runs
    vrand=np.sum(np.sum(crand,1)/float(timesfliped))/runs
    vmin=np.sum(cmin/float(timesfliped))/runs

    print("Average value of vmin= %s" % vmin)
    print("Average value of v1= %s" % v1)
    print("Average value of vrand= %s" % vrand)

    return 0
